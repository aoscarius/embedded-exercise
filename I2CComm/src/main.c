/**
 *  I2C communication test between two boards with polling technique and std_library use.
 */

#include "stm32f4xx.h"
#include "stm32f4xx_i2c.h"

// Referring to the Reference Manual

// Comment if you wont to be a slave
#define I2C_MASTER

#define I2C_SPEED 400000
#define I2C_SLAVE_ADDRESS 0x30

/* I2C Special Address:
	0000000 0 	 General Call
	0000000 1 	 Start Byte
	0000001 X 	 CBUS Addresses
	0000010 X 	 Reserved for Different Bus Formats
	0000011 X 	 Reserved for future purposes
	00001XX X 	 High-Speed Master Code
	11110XX X 	 10-bit Slave Addressing
	11111XX X 	 Reserved for future purposes
*/

#define COUNTOF(__BUFFER__)  (sizeof(__BUFFER__) / sizeof(*(__BUFFER__)))
#define BUFFER_SIZE          (COUNTOF(tx_data) - 1)

#define LED_ORANGE	GPIO_Pin_13	// LED Orange (Up)
#define LED_BLUE	GPIO_Pin_15	// LED Blue (Down)
#define LED_GREEN	GPIO_Pin_12	// LED Verde (Left)
#define LED_RED		GPIO_Pin_14	// LED Rosso (Right)

#define BUTTON		GPIO_Pin_0	// User Button

static uint8_t tx_data[]="01234567890123456789";
static uint8_t rx_data[BUFFER_SIZE];

#define TIMEOUTDEF  2000//ms

__IO uint32_t TTimeOut;
__IO uint32_t TDelay;

void SysTick_Handler(void){
	if(TTimeOut!=0){
		TTimeOut--;
	}
	if(TDelay!=0){
		TDelay--;
	}
}

void delay(uint32_t ms) {
    TDelay = ms;
    while(TDelay!=0);
}

void TTimeOut_Handler(void){
	while(1){
		GPIO_SetBits(GPIOD, LED_RED);
		delay(500);
		GPIO_ResetBits(GPIOD, LED_RED);
		delay(500);
	}
}

void GPIO_Initialize(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	/* GPIOD and GPIOA Peripheral clock enable */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOD, ENABLE);

	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = LED_ORANGE | LED_BLUE | LED_GREEN | LED_RED;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	GPIO_StructInit(&GPIO_InitStructure);
	/* Configure PD0 in input pushpull mode */
	GPIO_InitStructure.GPIO_Pin = BUTTON;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}

void I2C1_Init(void){

	GPIO_InitTypeDef GPIO_InitStruct;
	I2C_InitTypeDef I2C_InitStruct;

	// enable APB1 peripheral clock for I2C1
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
	// enable clock for SCL and SDA pins
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	// SCL on PB6 and SDA on PB9
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_9; 				// we are going to use PB6 and PB7
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;			// set pins to alternate function
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;		// set GPIO speed
	GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;			// set output to open drain --> the line has to be only pulled low, not driven high
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;			// disable pull up resistors
	GPIO_Init(GPIOB, &GPIO_InitStruct);					// init GPIOB

	// Connect I2C1 pins to AF
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_I2C1);	// SCL
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_I2C1); // SDA

	// configure I2C1
	I2C_InitStruct.I2C_ClockSpeed = I2C_SPEED; 		// 100kHz
	I2C_InitStruct.I2C_Mode = I2C_Mode_I2C;			// I2C mode
	I2C_InitStruct.I2C_DutyCycle = I2C_DutyCycle_2;	// 50% duty cycle --> standard

	#ifdef I2C_MASTER
	I2C_InitStruct.I2C_OwnAddress1 = 0x00;	// own address, not relevant in master mode
	#else
	I2C_InitStruct.I2C_OwnAddress1 = I2C_SLAVE_ADDRESS;	// own address, not relevant in master mode
	#endif

	I2C_InitStruct.I2C_Ack = I2C_Ack_Enable;		// disable acknowledge when reading (can be changed later on)
	I2C_InitStruct.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit; // set address length to 7 bit addresses
	I2C_Init(I2C1, &I2C_InitStruct);				// init I2C1

	// enable I2C1
	I2C_Cmd(I2C1, ENABLE);
}

#ifdef I2C_MASTER // --- Master Mode Functions ---

void I2C_MasterWrite(I2C_TypeDef* I2Cx, uint8_t I2C_Address, uint8_t* data, uint8_t length){
	// wait until I2C1 is not busy anymore
	while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY));

	// Send I2C1 START condition
	I2C_GenerateSTART(I2Cx, ENABLE);

	// wait for I2C1 EV5 --> Slave has acknowledged start condition
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_MODE_SELECT));

	// Send slave Address for write
	I2C_Send7bitAddress(I2Cx, I2C_Address, I2C_Direction_Transmitter);

	// wait for I2C1 EV6, check if Slave has acknowledged Master transmitter
	TTimeOut=TIMEOUTDEF;
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)&&(TTimeOut!=0x00));
	if(TTimeOut==0) TTimeOut_Handler();

	for (uint8_t i=0; i<length; i++) {
		// write data to I2C data register
		I2C_SendData(I2Cx, data[i]);
		// wait for I2C1 EV8_2 --> byte has been transmitted
		GPIO_SetBits(GPIOD, LED_BLUE);
		while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
		GPIO_ResetBits(GPIOD, LED_BLUE);
	}

	// Send I2C1 STOP Condition (this turn to Slave mode)
	I2C_GenerateSTOP(I2Cx, ENABLE);
}

void I2C_MasterRead(I2C_TypeDef* I2Cx, uint8_t I2C_Address, uint8_t* data, uint8_t length){
	// wait until I2C1 is not busy anymore
	while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY));

	// Send I2C1 START condition
	I2C_GenerateSTART(I2Cx, ENABLE);

	// wait for I2C1 EV5 --> Slave has acknowledged start condition
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_MODE_SELECT));

	// Send slave Address for write
	I2C_Send7bitAddress(I2Cx, I2C_Address, I2C_Direction_Receiver);

	// wait for I2C1 EV6, check if Slave has acknowledged Master receiver
	TTimeOut=TIMEOUTDEF;
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)&&(TTimeOut!=0x00));
	if(TTimeOut==0) TTimeOut_Handler();

	for (uint8_t i=0; i<length; i++) {
		if (i==length-1) {
			I2C_AcknowledgeConfig(I2Cx, DISABLE);
			I2C_GenerateSTOP(I2Cx, ENABLE);
		}
		// wait until one byte has been received
		GPIO_SetBits(GPIOD, LED_BLUE);
		while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED));
		GPIO_ResetBits(GPIOD, LED_BLUE);
		// read data from I2C data register
		data[i] = I2C_ReceiveData(I2Cx);
	}

	// Send I2C1 STOP Condition (this turn to Slave mode)
	I2C_GenerateSTOP(I2Cx, ENABLE);
}

#else // --- Slave Mode Functions ---

void I2C_SlaveRead(I2C_TypeDef* I2Cx, uint8_t* data, uint8_t length){
	// wait for I2C1 EV1, check if Slave has recognized own address
	TTimeOut=TIMEOUTDEF;
	while (!I2C_CheckEvent(I2Cx, I2C_EVENT_SLAVE_RECEIVER_ADDRESS_MATCHED)&&(TTimeOut!=0x00));
	if(TTimeOut==0) TTimeOut_Handler();

	// Control on stop condition for fault tolerance
	for (uint8_t i=0; (i<length) && !I2C_CheckEvent(I2Cx, I2C_EVENT_SLAVE_STOP_DETECTED); i++) {
		// wait until one byte has been received
		while(!I2C_CheckEvent(I2Cx, I2C_EVENT_SLAVE_BYTE_RECEIVED));
		// read data from I2C data register and return data byte
		data[i] = I2C_ReceiveData(I2Cx);
	}
}

void I2C_SlaveWrite(I2C_TypeDef* I2Cx, uint8_t* data, uint8_t length){
	// wait for I2C1 EV1, check if Slave has recognized own address
	TTimeOut=TIMEOUTDEF;
	while (!I2C_CheckEvent(I2Cx, I2C_EVENT_SLAVE_TRANSMITTER_ADDRESS_MATCHED)&&(TTimeOut!=0x00));
	if(TTimeOut==0) TTimeOut_Handler();

	// Control on stop condition for fault tolerance
	for (uint8_t i=0; (i<length) && !I2C_CheckEvent(I2Cx, I2C_EVENT_SLAVE_STOP_DETECTED); i++) {
		// write data to I2C data register
		I2C_SendData(I2Cx, data[i]);
		// wait for I2C1 EV3 --> byte has been transmitted
		while(!I2C_CheckEvent(I2Cx, I2C_EVENT_SLAVE_BYTE_TRANSMITTED));
	}
}

#endif

uint8_t BuffCmp(const uint8_t* b1, const uint8_t* b2, uint16_t length){
	while(--length){
		if (*b1++!=*b2++) return 0;
	}
	return 1;
}

int main(void){

	GPIO_Initialize();
	I2C1_Init(); // initialize I2C1 peripheral
	SysTick_Config((SystemCoreClock/1000)+10);

	#ifdef I2C_MASTER // -- Master Mode --

	// Pause for push-pull user button
	while (!GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0));
	while (GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0));

	I2C_MasterWrite(I2C1, I2C_SLAVE_ADDRESS, (uint8_t*)tx_data, BUFFER_SIZE);

	// Pause for push-pull user button
	while (!GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0));
	while (GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0));

	I2C_MasterRead(I2C1, I2C_SLAVE_ADDRESS, (uint8_t*)rx_data, BUFFER_SIZE);

	GPIO_ResetBits(GPIOD, LED_GREEN | LED_RED);
	if (BuffCmp((uint8_t*)tx_data, (uint8_t*)rx_data, BUFFER_SIZE)) {
		// Trasmissione andata a buon fine
		GPIO_SetBits(GPIOD, LED_GREEN);
	} else {
		// Trasmissione non terminata con successo
		GPIO_SetBits(GPIOD, LED_RED);
	}

	#else // -- Slave Mode --

	I2C_SlaveRead(I2C1, (uint8_t*)rx_data, BUFFER_SIZE);

	GPIO_ResetBits(GPIOD, LED_GREEN | LED_RED);
	if (BuffCmp((uint8_t*)tx_data, (uint8_t*)rx_data, BUFFER_SIZE)) {
		// Trasmissione andata a buon fine
		GPIO_SetBits(GPIOD, LED_GREEN);
	} else {
		// Trasmissione non terminata con successo
		GPIO_SetBits(GPIOD, LED_RED);
	}

	I2C_SlaveWrite(I2C1, (uint8_t*)tx_data, BUFFER_SIZE);

	#endif
	while(1){
	}
}

