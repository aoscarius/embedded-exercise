#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "lis3dsh.h"

#define LED_GREEN	GPIO_PIN_12	// LED Verde (Left)
#define LED_ORANGE 	GPIO_PIN_13	// LED Arancio (Up)
#define LED_RED		GPIO_PIN_14	// LED Rosso (Right)
#define LED_BLUE 	GPIO_PIN_15	// LED Blu (Down)

#define THREADHOLDLow  -500
#define THREADHOLDHigh  500

const uint16_t LEDS = LED_GREEN | LED_ORANGE | LED_RED | LED_BLUE;
const uint16_t LED[4] = {LED_GREEN, LED_ORANGE, LED_RED, LED_BLUE};

typedef enum
{	s_stop,
	s_rotate_left,
	s_rotate_right
} s_state;

int16_t accXYZBuff[3];

void LEDInit();
void LIS3DSHInit();

int main(void){

	uint8_t counter = 0;
	uint32_t delayValue = 0;
	uint32_t delayIncFactor = 0;

	s_state curr_state = s_stop;

	/* SysTick end of count event each 1ms */
	SystemCoreClock = HAL_RCC_GetHCLKFreq();
	SysTick_Config(SystemCoreClock / 1000);

	HAL_Init();
	LEDInit();
	LIS3DSHInit();

	while(1){
	// FSM Definition
		switch (curr_state){
		case s_stop:
			if (accXYZBuff[0] < THREADHOLDLow) {
				delayValue = 0;
				delayIncFactor = (uint32_t)((5000/abs(accXYZBuff[0]))+1);
				curr_state = s_rotate_right;
			} else if (accXYZBuff[0] > THREADHOLDHigh) {
				delayValue = 0;
				delayIncFactor = (uint32_t)((5000/abs(accXYZBuff[0]))+1);
				curr_state = s_rotate_left;
			}
			break;
		case s_rotate_left:
			counter--;
			delayValue+=delayIncFactor;
			HAL_GPIO_WritePin(GPIOD, LEDS, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOD, LED[counter % 4], GPIO_PIN_SET);
			HAL_Delay(delayValue);
			if (delayValue>250) curr_state = s_stop;
			break;
		case s_rotate_right:
			counter++;
			delayValue+=delayIncFactor;
			HAL_GPIO_WritePin(GPIOD, LEDS, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOD, LED[counter % 4], GPIO_PIN_SET);
			HAL_Delay(delayValue);
			if (delayValue>250) curr_state = s_stop;
			break;
		default:
			HAL_GPIO_WritePin(GPIOD, LEDS, GPIO_PIN_RESET);
			break;
		}
	}
}

void LEDInit(){
	GPIO_InitTypeDef GPIO_InitStruct;

	/* Enable the GPIO_LED Clock */
	__GPIOD_CLK_ENABLE();

	/* Configure the GPIO_LED pins */
	GPIO_InitStruct.Pin = LEDS;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FAST;

	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOD, LEDS, GPIO_PIN_RESET);
}

void LIS3DSHInit(){
	uint16_t ctrl = 0x0000;
	LIS3DSH_InitTypeDef l1s3dsh_InitStruct;

	/* Set configuration of LIS3DSH MEMS Accelerometer **********************/
	l1s3dsh_InitStruct.Output_DataRate = LIS3DSH_DATARATE_100;
	l1s3dsh_InitStruct.Axes_Enable = LIS3DSH_XYZ_ENABLE;
	l1s3dsh_InitStruct.SPI_Wire = LIS3DSH_SERIALINTERFACE_4WIRE;
	l1s3dsh_InitStruct.Self_Test = LIS3DSH_SELFTEST_NORMAL;
	l1s3dsh_InitStruct.Full_Scale = LIS3DSH_FULLSCALE_6;
	l1s3dsh_InitStruct.Filter_BW = LIS3DSH_FILTER_BW_800;

	/* Configure MEMS: power mode(ODR) and axes enable */
	ctrl = (uint16_t) (l1s3dsh_InitStruct.Output_DataRate | \
				   l1s3dsh_InitStruct.Axes_Enable);

	/* Configure MEMS: full scale and self test */
	ctrl |= (uint16_t) ((l1s3dsh_InitStruct.SPI_Wire    | \
					 l1s3dsh_InitStruct.Self_Test   | \
					 l1s3dsh_InitStruct.Full_Scale  | \
					 l1s3dsh_InitStruct.Filter_BW) << 8);

	/* Configure the accelerometer main parameters */
	LIS3DSH_Init(ctrl);
}

void HAL_SYSTICK_Callback(void){
	LIS3DSH_ReadACC(accXYZBuff);
}
