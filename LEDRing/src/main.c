/**
* University of Naples Federico II
* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*/

/**
 * Questo progetto implementa una semplice macchina a stati finiti per effettuare
 * diversi giochi di luce sui LED montati sulla board, così da testare il controllo
 * del GPIO sul SOC STM32F407VG in modalità bare metal.
 */

// Dependency: stdperiph_lib (Standard Library v1.1.0 STM32)

#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

// Definition of mnemonic macro
#define LED_GREEN	GPIO_Pin_12	// LED Verde (Left)
#define LED_ORANGE 	GPIO_Pin_13	// LED Arancio (Up)
#define LED_RED		GPIO_Pin_14	// LED Rosso (Right)
#define LED_BLUE 	GPIO_Pin_15	// LED Blue (Down)

#define BUTTON		GPIO_Pin_0	// User Button

const uint16_t LEDS = LED_GREEN | LED_ORANGE | LED_RED | LED_BLUE;
const uint16_t LED[4] = {LED_GREEN, LED_ORANGE, LED_RED, LED_BLUE};

GPIO_InitTypeDef  GPIO_InitStructure;

void GPIO_Initialize(void);
void delay(uint32_t ms);

// Used for intercept the rising edge of the button
// This is without interrupt, for experiment
static uint8_t previous;

// States definition of our state machine
typedef enum
{	s_rotate_left,
	s_rotate_right,
	s_updown_blink,
	s_leftright_blink,
	s_disco_blink
} s_state;

// Start state
s_state curr_state = s_rotate_left;

int main(void)
{
	static uint32_t counter = 0;
	static int8_t state_select = 0;

	GPIO_Initialize();

	/* Main Loop */
	while (1)
	{
		/* If the button is pressed the next state of the state
		 * machine is selected.
		 */
		// Falling edge detect
		if (GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0)==0){
			if (previous==1){
				state_select = (state_select + 1) % 5;
				curr_state = state_select;
			}
			previous=0;
		} else {
			previous=1;
		}

		// FSM Definition
		switch (curr_state){
		case s_rotate_left:
			// In the rotate_left state the led blink in a turn left ring effect
			counter--;
			GPIO_ResetBits(GPIOD, LEDS);
			GPIO_SetBits(GPIOD, LED[counter % 4]);
			delay(1000);
			break;
		case s_rotate_right:
			// In the rotate_right state the led blink in a turn right ring effect
			counter++;
			GPIO_ResetBits(GPIOD, LEDS);
			GPIO_SetBits(GPIOD, LED[counter % 4]);
			delay(1000);
			break;
		case s_updown_blink:
			// In the updown_blink state the led up and down blink
			GPIO_ResetBits(GPIOD, LEDS);
			delay(500);
			GPIO_SetBits(GPIOD, LED_ORANGE | LED_BLUE);
			delay(500);
			break;
		case s_leftright_blink:
			// In the leftright_blink state the led left and right blink
			GPIO_ResetBits(GPIOD, LEDS);
			delay(500);
			GPIO_SetBits(GPIOD, LED_GREEN | LED_RED);
			delay(500);
			break;
		case s_disco_blink:
			// In the disco_blink state the led up and down blink and after the led left and right blink
			GPIO_ResetBits(GPIOD, LEDS);
			delay(250);
			GPIO_SetBits(GPIOD, LED_ORANGE | LED_BLUE);
			delay(250);
			GPIO_ResetBits(GPIOD, LEDS);
			delay(250);
			GPIO_SetBits(GPIOD, LED_GREEN | LED_RED);
			delay(250);
			break;
		default:
			GPIO_ResetBits(GPIOD, LEDS);
			break;
		}
	}
}

/**
 * @biref 	A simple, low precision, delay function. Its purpose is
 * 			to consume empty clock cycles.
 * @params	Delay time in milliseconds
 * @retval	None
 */
void delay(uint32_t ms) {
	// First attempt of low precision delay function
    ms *= 3360;
    while(ms--) {
        __NOP();
    }
}

/**
 * @biref 	This function initialize the GPIO port linked to the LEDs
 *          and the BUTTON to the board for output and input mode respectively.
 * @params	None
 * @retval	None
 */
void GPIO_Initialize(void)
{
	/* GPIOD and GPIOA Peripheral clock enable */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOD, ENABLE);

	GPIO_StructInit(&GPIO_InitStructure);
	/* Configure PD12 and PD13 in output pushpull mode */
	GPIO_InitStructure.GPIO_Pin = LEDS;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	GPIO_StructInit(&GPIO_InitStructure);
	/* Configure PD0 in input pushpull mode */
	GPIO_InitStructure.GPIO_Pin = BUTTON;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}
