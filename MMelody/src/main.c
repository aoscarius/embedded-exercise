/**
* University of Naples Federico II
* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*/

/**
 * Questo progetto implementa la funzionalità di buzzer per permettere alla board di
 * suonare semplici melodie monofoniche. Per farlo, sfrutta i timer TIM presenti sul
 * SOC STM32F407VG ed in particolare la funzione PWM di tali timer. Infatti, modulando
 * opportunamente in frequenza un onda PWM, è possibile pilotare un piccolo piezo-buzzer
 * o un piccolo artoparlante da cellulare, per riprodurre diversi toni musicali che,
 * opportunamente riprodotti in sequenza e secondo precisi tempi, permettono di generare
 * melodie.
 * Si implementa in oltre una versione molto piu precisa della funzione di Delay, che
 * sfrutta l'interrupt hardware SysTick per generare pause di millisecondi in maniera più
 * accurata.
 *
 * Il tutto viene generato sul timer TIM4 collegato allo stesso GPIO dei LED e riprodotto
 * sul pin collegato al led GREEN, per poter osservare (oltre che ascoltare) la modulazione.
 *
 * E' necessario il seguente hardware aggiuntivo per poter udire i toni generati. Sono integrate
 * le melodie tratte dai giochi storici The Secret of Monkey Island e Tetris Theme Long Version.
 *
 * VDD -------------
 *                  |+
 *            BUZZ | | )))
 *                  |
 *                |/
 * PB12 -\/\/\----|
 *      500 Ohm   |\
 *                  |
 *                  |
 * GND -------------
 */

// Dependency: stdperiph_lib (Standard Library v1.1.0 STM32)

#include "stm32f4xx.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_tim.h"
#include "notes.h"

// This macro count the number of element into array
#define ARRAYCOUNT(_ARRAY_) (sizeof(_ARRAY_)/sizeof(*(_ARRAY_)))

// -- The Secret of Monkey Island Melody --
// main notes in the melody:
uint16_t monkey_melody[] = {
	0, NOTE_B3, NOTE_E4, NOTE_G4, 0,  NOTE_B3, NOTE_E4, NOTE_G4, 0, NOTE_D3, 0, NOTE_D4, 0, NOTE_A3, NOTE_D4,  NOTE_FS4, NOTE_D3, 0, NOTE_A3, NOTE_D4, NOTE_FS4, NOTE_D3, 0, NOTE_E3,  0, NOTE_B3, NOTE_E4, NOTE_G4, 0, NOTE_B3, NOTE_E4, NOTE_G4, 0, NOTE_B4,  NOTE_E5, 0, NOTE_G4, 0, NOTE_B3, NOTE_E4, NOTE_G4, NOTE_E5, 0, NOTE_B3,  NOTE_E4, NOTE_G4, NOTE_G5, NOTE_FS5, 0, NOTE_E5, NOTE_A3, NOTE_D4,   NOTE_FS4, NOTE_D5, NOTE_A3, NOTE_D4, NOTE_FS4, 0, 0, 0, NOTE_E5,   NOTE_G3, NOTE_C4, NOTE_E4, NOTE_C3, 0, NOTE_G3, NOTE_C4, NOTE_E4,   NOTE_C3, NOTE_G2, 0, NOTE_G3, NOTE_B3, NOTE_D4, NOTE_D5, NOTE_G3,  NOTE_B3, NOTE_D4, 0, NOTE_G4, NOTE_D5, NOTE_C5, NOTE_G3, NOTE_B3,   NOTE_D4, NOTE_B4, NOTE_G3, NOTE_B3, NOTE_D4, NOTE_D5, NOTE_C5, 0,   NOTE_C4, 0, NOTE_A3, NOTE_C4, NOTE_E4, NOTE_C5, NOTE_A3, NOTE_C4,   NOTE_E4, 0, NOTE_B4, 0, NOTE_G3, NOTE_B3, NOTE_E4, NOTE_E2, 0, 0, 0,   NOTE_B3, NOTE_E4, NOTE_E2, 0, NOTE_B4, NOTE_E5, NOTE_E2, 0, NOTE_G5,   0, 0, NOTE_B5, NOTE_E2, 0, NOTE_E4, 0, NOTE_E5, NOTE_B3, NOTE_E4, NOTE_G4,  NOTE_E5, 0, NOTE_B3, NOTE_E4, NOTE_G4, 0, NOTE_B4, NOTE_E5, NOTE_B3,   NOTE_E4, NOTE_G4, 0, NOTE_B3, NOTE_E4, NOTE_G4, NOTE_G5, NOTE_FS5,   NOTE_E5, NOTE_A3, NOTE_D4, NOTE_FS4, NOTE_D5, NOTE_A3, NOTE_D4, NOTE_FS4,   0, NOTE_C3, 0, NOTE_E5, 0, NOTE_G3, NOTE_C4, NOTE_E4, 0, NOTE_C3, 0,  NOTE_G3, NOTE_C4, NOTE_E4, NOTE_C3, 0, NOTE_E4, 0, NOTE_G3, NOTE_C4,   NOTE_E4, 0, NOTE_G3, NOTE_C4, NOTE_E4, NOTE_FS5, NOTE_G5, 0, NOTE_B3,   0, NOTE_G3, NOTE_B3, NOTE_D4, NOTE_G5, 0, NOTE_G5, NOTE_G3, NOTE_B3,   NOTE_D4, 0, NOTE_A4, NOTE_A5, NOTE_A3, NOTE_A5, NOTE_C4, 0, NOTE_A3,   NOTE_C4, 0, NOTE_FS5, NOTE_D3, NOTE_A3, NOTE_D4, NOTE_FS4, NOTE_D3, 0,   NOTE_A3, NOTE_D4, NOTE_FS4, 0, NOTE_G5, NOTE_FS5, 0, NOTE_E5, NOTE_A3,   NOTE_D4, NOTE_FS4, NOTE_D5, NOTE_FS5, NOTE_A3, NOTE_D4, NOTE_FS4,   NOTE_FS5, NOTE_G5, 0, NOTE_G5, 0, NOTE_G2, 0, NOTE_G5, NOTE_B3, NOTE_D4,   NOTE_G4, NOTE_G5, NOTE_B3, NOTE_D4, NOTE_G4, 0, NOTE_FS5, NOTE_B2, NOTE_B3,   NOTE_DS4, NOTE_FS4, NOTE_B2, 0, NOTE_B3, NOTE_DS4, NOTE_FS4, 0, NOTE_E5,   NOTE_E3, NOTE_B3, NOTE_E4, NOTE_G4, NOTE_E3, 0, NOTE_B3, NOTE_E4, NOTE_G4,  NOTE_G5, 0, NOTE_G5, NOTE_FS5, NOTE_E5, NOTE_A3, NOTE_D4, NOTE_FS4, NOTE_D5,   NOTE_A3, NOTE_D4, NOTE_FS4, NOTE_FS5, NOTE_G5, 0, NOTE_G2, NOTE_B3, NOTE_D4,   NOTE_G4, NOTE_G5, NOTE_B3, NOTE_D4, NOTE_G4, 0, 0, NOTE_FS5, NOTE_B2, NOTE_B3,   NOTE_DS4, NOTE_FS4, 0, NOTE_B3, NOTE_DS4, NOTE_FS4, 0, NOTE_E5, NOTE_E3, NOTE_B3,   NOTE_E4, NOTE_G4, 0, NOTE_B3, NOTE_E4, NOTE_G4, NOTE_G5, NOTE_FS5, 0, NOTE_E5,   NOTE_A3, NOTE_D4, NOTE_FS4, NOTE_D5, NOTE_A3, NOTE_D4, NOTE_FS4, 0, NOTE_E5, 0,   NOTE_C3, NOTE_G3, NOTE_C4, NOTE_E4, NOTE_E5, NOTE_G3, NOTE_C4, NOTE_E4, 0, NOTE_E5,   NOTE_G3, NOTE_C4, NOTE_E4, NOTE_C3, 0, NOTE_G3, NOTE_C4, NOTE_E4, 0, NOTE_C3, 0,   NOTE_G4, 0, NOTE_B3, 0, NOTE_E5, NOTE_G3, NOTE_C4, NOTE_E4, NOTE_E5, 0, NOTE_G3,   NOTE_C4, NOTE_E4, 0, NOTE_D5, NOTE_C5, NOTE_G3, NOTE_B3, NOTE_D4, NOTE_B4, NOTE_G3,   NOTE_B3, NOTE_D4, NOTE_D5, NOTE_C5, NOTE_A2, 0, NOTE_A3, NOTE_A4, NOTE_C4, 0,   NOTE_C5, NOTE_A3, NOTE_C5, NOTE_C4, NOTE_A2, 0, NOTE_B4, NOTE_E2, NOTE_B3,   NOTE_E4, NOTE_G4, 0, NOTE_B3, NOTE_E4, NOTE_G4, NOTE_E2, 0, NOTE_B4, 0, NOTE_G4,   0, NOTE_B3, NOTE_E4, NOTE_G4, 0, NOTE_B3, NOTE_E4, NOTE_G4, 0, NOTE_D3, 0,   NOTE_B2, NOTE_A3, NOTE_D4, NOTE_FS4, NOTE_D3, 0, NOTE_A3, NOTE_D4, NOTE_FS4, 0,   NOTE_B4, 0, NOTE_E5, 0, NOTE_G5, 0, NOTE_B5, 0, NOTE_B5, 0, NOTE_B5, NOTE_E3, 0,   NOTE_E4, 0, NOTE_B3, NOTE_E4, NOTE_G4, NOTE_E3, 0, NOTE_B3, NOTE_E4, NOTE_G4,   NOTE_E3, 0, NOTE_G4, 0, NOTE_B3, NOTE_E4, NOTE_G4, 0, NOTE_B3
};
// note durations in milliseconds
uint16_t monkey_noteDurations[] = {
	137, 12, 12, 18, 123, 12, 12, 18, 135,  14, 155, 14, 139, 14, 10, 19, 41, 81, 14, 10, 19, 116, 19, 249, 68, 12, 12, 18, 123,  12, 12, 18, 135, 8, 135, 21, 15, 137, 12, 12, 18, 109, 13, 12, 12, 20, 141, 177, 4,  126, 14, 14, 16, 123, 14, 10, 16, 137, 8, 5, 305, 10, 15, 15, 84, 39, 10, 15, 18,  145, 286, 20, 10, 16, 17, 121, 15, 12, 14, 137, 8, 145, 164, 15, 16, 14, 119, 15,  16, 17, 129, 164, 9, 15, 137, 14, 11, 18, 122, 14, 11, 15, 119, 249, 89, 10, 16,  15, 36, 82, 10, 5, 16, 15, 24, 8, 6, 17, 36, 6, 11, 12, 7, 10, 24, 137, 15, 128,  9, 12, 12, 18, 74, 48, 12, 12, 18, 135, 8, 308, 12, 12, 18, 123, 12, 9, 18, 143,  169, 144, 14, 10, 19, 121, 14, 14, 13, 137, 15, 5, 287, 10, 10, 15, 15, 10, 92,  18, 10, 15, 18, 176, 127, 15, 137, 10, 15, 15, 124, 10, 15, 18, 116, 164, 22, 16,  136, 10, 12, 17, 28, 1, 96, 15, 12, 14, 137, 5, 312, 18, 7, 15, 124, 18, 23, 166,  173, 123, 14, 10, 19, 20, 102, 14, 7, 16, 3, 143, 143, 21, 152, 9, 17, 16, 111, 8,  14, 10, 19, 122, 3, 1, 164, 18, 133, 6, 13, 12, 14, 18, 121, 12, 14, 15, 137, 220,  105, 8, 13, 16, 32, 91, 12, 13, 16, 124, 204, 134, 12, 15, 15, 12, 111, 12, 12, 20,  125, 1, 6, 169, 152, 14, 14, 16, 123, 9, 17, 16, 143, 181, 17, 122, 8, 10, 18, 125,  12, 14, 18, 1, 157, 173, 130, 12, 10, 16, 124, 12, 13, 16, 166, 169, 128, 12, 12, 15,  125, 12, 12, 20, 133, 169, 4, 152, 14, 14, 16, 121, 14, 14, 14, 146, 155, 39, 115, 15,  8, 21, 125, 10, 15, 15, 145, 317, 10, 15, 15, 76, 47, 10, 15, 15, 6, 99, 32, 15, 154,  16, 132, 5, 10, 15, 18, 108, 13, 10, 15, 15, 137, 177, 143, 15, 12, 17, 121, 10, 12,  17, 163, 160, 118, 12, 18, 7, 15, 6, 118, 18, 8, 15, 118, 56, 178, 109, 12, 12, 15,  125, 12, 12, 18, 122, 14, 16, 153, 15, 137, 12, 12, 18, 123, 12, 12, 18, 135, 14,  155, 154, 9, 14, 19, 102, 19, 14, 10, 16, 32, 16, 18, 17, 9, 17, 17, 3, 1, 9, 1, 3,  146, 10, 15, 137, 12, 9, 18, 30, 95, 12, 12, 18, 152, 152, 15, 137, 12, 12, 18, 123,  12
};

// -- Tetris Theme Long Melody with Bass --
// main notes in the melody:
uint16_t tetris_melody[] = {
	0,  NOTE_E5, NOTE_E3, NOTE_B4, NOTE_C5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_C5, NOTE_B4, NOTE_A4, NOTE_A3, NOTE_A4, NOTE_C5, NOTE_E5, NOTE_A3, NOTE_D5,  NOTE_C5, NOTE_B4, NOTE_E4, NOTE_G4, NOTE_C5, NOTE_D5, NOTE_E3, NOTE_E5,  NOTE_E3, NOTE_C5, NOTE_A3, NOTE_A4, NOTE_A3, NOTE_A4, NOTE_A3, NOTE_B2, NOTE_C3, NOTE_D3, NOTE_D5, NOTE_F5, NOTE_A5, NOTE_C5, NOTE_C5, NOTE_G5, NOTE_F5, NOTE_E5, NOTE_C3, 0, NOTE_C5, NOTE_E5, NOTE_A4, NOTE_G4, NOTE_D5,  NOTE_C5, NOTE_B4, NOTE_E4, NOTE_B4, NOTE_C5, NOTE_D5, NOTE_G4, NOTE_E5, NOTE_G4, NOTE_C5, NOTE_E4, NOTE_A4, NOTE_E3, NOTE_A4, 0, NOTE_E5, NOTE_E3, NOTE_B4, NOTE_C5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_C5, NOTE_B4, NOTE_A4, NOTE_A3, NOTE_A4, NOTE_C5, NOTE_E5, NOTE_A3, NOTE_D5,  NOTE_C5, NOTE_B4, NOTE_E4, NOTE_G4, NOTE_C5, NOTE_D5, NOTE_E3, NOTE_E5,  NOTE_E3, NOTE_C5, NOTE_A3, NOTE_A4, NOTE_A3, NOTE_A4, NOTE_A3, NOTE_B2, NOTE_C3, NOTE_D3, NOTE_D5, NOTE_F5, NOTE_A5, NOTE_C5, NOTE_C5, NOTE_G5, NOTE_F5, NOTE_E5, NOTE_C3, 0, NOTE_C5, NOTE_E5, NOTE_A4, NOTE_G4, NOTE_D5,  NOTE_C5, NOTE_B4, NOTE_E4, NOTE_B4, NOTE_C5, NOTE_D5, NOTE_G4, NOTE_E5, NOTE_G4, NOTE_C5, NOTE_E4, NOTE_A4, NOTE_E3, NOTE_A4, 0,  NOTE_E4, NOTE_E3, NOTE_A2, NOTE_E3, NOTE_C4, NOTE_E3, NOTE_A2, NOTE_E3, NOTE_D4, NOTE_E3, NOTE_GS2, NOTE_E3, NOTE_B3, NOTE_E3, NOTE_GS2, NOTE_E3,  NOTE_C4, NOTE_E3, NOTE_A2, NOTE_E3, NOTE_A3, NOTE_E3, NOTE_A2, NOTE_E3, NOTE_GS3, NOTE_E3, NOTE_GS2, NOTE_E3, NOTE_B3, NOTE_E3, NOTE_GS2, NOTE_E3, NOTE_E4, NOTE_E3, NOTE_A2, NOTE_E3, NOTE_C4, NOTE_E3, NOTE_A2, NOTE_E3, NOTE_D4, NOTE_E3, NOTE_GS2, NOTE_E3, NOTE_B3, NOTE_E3, NOTE_GS2, NOTE_E3,  NOTE_C4, NOTE_E3, NOTE_E4, NOTE_E3, NOTE_A4, NOTE_E3, NOTE_A2, NOTE_E3, NOTE_GS4, NOTE_E3, NOTE_GS2, NOTE_E3, NOTE_GS2, NOTE_E3, NOTE_GS2, NOTE_E3,  NOTE_E5, NOTE_E3, NOTE_B4, NOTE_C5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_C5, NOTE_B4, NOTE_A4, NOTE_A3, NOTE_A4, NOTE_C5, NOTE_E5, NOTE_A3, NOTE_D5,  NOTE_C5, NOTE_B4, NOTE_E4, NOTE_G4, NOTE_C5, NOTE_D5, NOTE_E3, NOTE_E5,  NOTE_E3, NOTE_C5, NOTE_A3, NOTE_A4, NOTE_A3, NOTE_A4, NOTE_A3, NOTE_B2, NOTE_C3, NOTE_D3, NOTE_D5, NOTE_F5, NOTE_A5, NOTE_C5, NOTE_C5, NOTE_G5, NOTE_F5, NOTE_E5, NOTE_C3, 0, NOTE_C5, NOTE_E5, NOTE_A4, NOTE_G4, NOTE_D5,  NOTE_C5, NOTE_B4, NOTE_E4, NOTE_B4, NOTE_C5, NOTE_D5, NOTE_G4, NOTE_E5, NOTE_G4, NOTE_C5, NOTE_E4, NOTE_A4, NOTE_E3, NOTE_A4, 0, NOTE_E5, NOTE_E3, NOTE_B4, NOTE_C5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_C5, NOTE_B4, NOTE_A4, NOTE_A3, NOTE_A4, NOTE_C5, NOTE_E5, NOTE_A3, NOTE_D5,  NOTE_C5, NOTE_B4, NOTE_E4, NOTE_G4, NOTE_C5, NOTE_D5, NOTE_E3, NOTE_E5,  NOTE_E3, NOTE_C5, NOTE_A3, NOTE_A4, NOTE_A3, NOTE_A4, NOTE_A3, NOTE_B2, NOTE_C3, NOTE_D3, NOTE_D5, NOTE_F5, NOTE_A5, NOTE_C5, NOTE_C5, NOTE_G5, NOTE_F5, NOTE_E5, NOTE_C3, 0, NOTE_C5, NOTE_E5, NOTE_A4, NOTE_G4, NOTE_D5,  NOTE_C5, NOTE_B4, NOTE_E4, NOTE_B4, NOTE_C5, NOTE_D5, NOTE_G4, NOTE_E5, NOTE_G4, NOTE_C5, NOTE_E4, NOTE_A4, NOTE_E3, NOTE_A4, 0,  NOTE_E4, NOTE_E3, NOTE_A2, NOTE_E3, NOTE_C4, NOTE_E3, NOTE_A2, NOTE_E3, NOTE_D4, NOTE_E3, NOTE_GS2, NOTE_E3, NOTE_B3, NOTE_E3, NOTE_GS2, NOTE_E3,  NOTE_C4, NOTE_E3, NOTE_A2, NOTE_E3, NOTE_A3, NOTE_E3, NOTE_A2, NOTE_E3, NOTE_GS3, NOTE_E3, NOTE_GS2, NOTE_E3, NOTE_B3, NOTE_E3, NOTE_GS2, NOTE_E3, NOTE_E4, NOTE_E3, NOTE_A2, NOTE_E3, NOTE_C4, NOTE_E3, NOTE_A2, NOTE_E3, NOTE_D4, NOTE_E3, NOTE_GS2, NOTE_E3, NOTE_B3, NOTE_E3, NOTE_GS2, NOTE_E3,  NOTE_C4, NOTE_E3, NOTE_E4, NOTE_E3, NOTE_A4, NOTE_E3, NOTE_A2, NOTE_E3, NOTE_GS4, NOTE_E3, NOTE_GS2, NOTE_E3, NOTE_GS2, NOTE_E3, NOTE_GS2, NOTE_E3
};
// note durations: 4 = quarter note, 8 = eighth note, etc
uint16_t tetris_noteDurations[] = {
	8, 16, 8, 8, 8, 8, 16, 16, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 4, 8, 8, 16, 16, 8, 8, 8, 8, 8, 8, 8, 16, 16, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 4, 4, 8, 8, 8, 8, 8, 16, 16, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 4, 8, 8, 16, 16, 8, 8, 8, 8, 8, 8, 8, 16, 16, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 4, 4, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 16, 16, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 4, 8, 8, 16, 16, 8, 8, 8, 8, 8, 8, 8, 16, 16, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 4, 4, 8, 8, 8, 8, 8, 16, 16, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 4, 8, 8, 16, 16, 8, 8, 8, 8, 8, 8, 8, 16, 16, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 4, 4, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8
};

// -- Pirates of the Caribbean Melody --
// main notes in the melody:
uint16_t pirates_melody[] = {
	NOTE_E4, NOTE_G4, NOTE_A4, NOTE_A4, 0, NOTE_A4, NOTE_B4, NOTE_C5, NOTE_C5, 0, NOTE_C5, NOTE_D5, NOTE_B4, NOTE_B4, 0, NOTE_A4, NOTE_G4, NOTE_A4, 0, NOTE_E4, NOTE_G4, NOTE_A4, NOTE_A4, 0, NOTE_A4, NOTE_B4, NOTE_C5, NOTE_C5, 0, NOTE_C5, NOTE_D5, NOTE_B4, NOTE_B4, 0, NOTE_A4, NOTE_G4, NOTE_A4, 0, NOTE_E4, NOTE_G4, NOTE_A4, NOTE_A4, 0, NOTE_A4, NOTE_C5, NOTE_D5, NOTE_D5, 0, NOTE_D5, NOTE_E5, NOTE_F5, NOTE_F5, 0, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_A4, 0, NOTE_A4, NOTE_B4, NOTE_C5, NOTE_C5, 0, NOTE_D5, NOTE_E5, NOTE_A4, 0, NOTE_A4, NOTE_C5, NOTE_B4, NOTE_B4, 0, NOTE_C5, NOTE_A4, NOTE_B4, 0, NOTE_A4, NOTE_A4, NOTE_A4, NOTE_B4, NOTE_C5, NOTE_C5, 0, NOTE_C5, NOTE_D5, NOTE_B4, NOTE_B4, 0, NOTE_A4, NOTE_G4, NOTE_A4, 0, NOTE_E4, NOTE_G4, NOTE_A4, NOTE_A4, 0, NOTE_A4, NOTE_B4, NOTE_C5, NOTE_C5, 0, NOTE_C5, NOTE_D5, NOTE_B4, NOTE_B4, 0, NOTE_A4, NOTE_G4, NOTE_A4, 0, NOTE_E4, NOTE_G4, NOTE_A4, NOTE_A4, 0, NOTE_A4, NOTE_C5, NOTE_D5, NOTE_D5, 0, NOTE_D5, NOTE_E5, NOTE_F5, NOTE_F5, 0, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_A4, 0, NOTE_A4, NOTE_B4, NOTE_C5, NOTE_C5, 0, NOTE_D5, NOTE_E5, NOTE_A4, 0, NOTE_A4, NOTE_C5, NOTE_B4, NOTE_B4, 0, NOTE_C5, NOTE_A4, NOTE_B4, 0, NOTE_E5, 0, 0, NOTE_F5, 0, 0, NOTE_E5, NOTE_E5, 0, NOTE_G5, 0, NOTE_E5, NOTE_D5, 0, 0, NOTE_D5, 0, 0, NOTE_C5, 0, 0, NOTE_B4, NOTE_C5, 0, NOTE_B4, 0, NOTE_A4, NOTE_E5, 0, 0, NOTE_F5, 0, 0, NOTE_E5, NOTE_E5, 0, NOTE_G5, 0, NOTE_E5, NOTE_D5, 0, 0, NOTE_D5, 0, 0, NOTE_C5, 0, 0, NOTE_B4, NOTE_C5, 0, NOTE_B4, 0, NOTE_A4
};
uint16_t pirates_noteDurations[] = {
	125, 125, 250, 125, 125, 125, 125, 250, 125, 125, 125, 125, 250, 125, 125, 125, 125, 375, 125, 125, 125, 250, 125, 125, 125, 125, 250, 125, 125, 125, 125, 250, 125, 125, 125, 125, 375, 125, 125, 125, 250, 125, 125, 125, 125, 250, 125, 125, 125, 125, 250, 125, 125, 125, 125, 125, 250, 125, 125, 125, 250, 125, 125, 250, 125, 250, 125, 125, 125, 250, 125, 125, 125, 125, 375, 375, 250, 125, 125, 125, 250, 125, 125, 125, 125, 250, 125, 125, 125, 125, 375, 125, 125, 125, 250, 125, 125, 125, 125, 250, 125, 125, 125, 125, 250, 125, 125, 125, 125, 375, 125, 125, 125, 250, 125, 125, 125, 125, 250, 125, 125, 125, 125, 250, 125, 125, 125, 125, 125, 250, 125, 125, 125, 250, 125, 125, 250, 125, 250, 125, 125, 125, 250, 125, 125, 125, 125, 375, 375, 250, 125, 375, 250, 125, 375, 125, 125, 125, 125, 125, 125, 125, 125, 375, 250, 125, 375, 250, 125, 375, 125, 125, 125, 125, 125, 500, 250, 125, 375, 250, 125, 375, 125, 125, 125, 125, 125, 125, 125, 125, 375, 250, 125, 375, 250, 125, 375, 125, 125, 125, 125, 125, 500
};

static TIM_TimeBaseInitTypeDef BUZZER_TimeBaseStructure;
static TIM_OCInitTypeDef BUZZER_OCInitStructure;

uint32_t TDelay;

/**
 * @brief	This function implement the handler fot the SysTick interrupt. It count
 * 			the millisecond that passed from the set of TDelay variable and the 0. It work
 * 			if the SysTick interrupt is set correctly to 1ms trough the SysTick_Config.
 * @param	None
 * @retval	None
 */
void SysTick_Handler(void){
	if(TDelay!=0){
		TDelay--;
	}
}

/**
 * @brief	This implement a precise delay function in milliseconds.
 * @param	Time in milliseconds
 * @retval	None
 */
void delay(uint32_t ms) {
    TDelay = ms;
    while(TDelay!=0);
}

/**
 * @brief	This function change the frequency of the TIM and set the duty cycle of the PWM
 * 			to 50% trough the selected period.
 * @param	freq is the frequency of the note to play
 * @param	duration is the duration of the note to play on and off
 * @retval	None
 */
void BUZZER_SetFrequency(uint16_t freq, uint16_t duration)
{

	// Calculate the period
	uint16_t b_period = SystemCoreClock / (freq * TIM_GetPrescaler(TIM4)) - 1;

	// Set the period of the timer
	BUZZER_TimeBaseStructure.TIM_Period = b_period;

	// Reinitialize the timer with the new frequency
	TIM_TimeBaseInit(TIM4, &BUZZER_TimeBaseStructure);

	// Set the duty cycle
	BUZZER_OCInitStructure.TIM_Pulse = b_period >> 1;

	// Reinitialize the comparator with the new duty cycle
	TIM_OC1Init(TIM4, &BUZZER_OCInitStructure);
	TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Enable);

	/* Start and Stop count on TIM4 for duration */
    TIM_Cmd(TIM4, ENABLE);
    delay(duration);
    TIM_Cmd(TIM4, DISABLE);
}

/**
 * @brief	Initialization of the GPIO port connected to the TIM4 and the BUTTON.
 * @param	None
 * @retval	None
 */
void PORT_Init(void) {
    GPIO_InitTypeDef GPIO_InitStructure;

    /* Clock for GPIOD */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD | RCC_AHB1Periph_GPIOA, ENABLE);
    /* Alternating functions for pins */
    GPIO_PinAFConfig(GPIOD, GPIO_PinSource12, GPIO_AF_TIM4);

    /* Set pins */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_Init(GPIOD, &GPIO_InitStructure);

	/* Configure PA0 in input pushpull mode */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}

/**
 * @brief	Initialization of the timer TIM4.
 * @param	None
 * @retval	None
 */
void TIMER_Init(void) {
    /* Enable clock for TIM4 */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);

    BUZZER_TimeBaseStructure.TIM_Prescaler = SystemCoreClock / 4000000;
    BUZZER_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    BUZZER_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    BUZZER_TimeBaseStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM4, &BUZZER_TimeBaseStructure);
}

/**
 * @brief	Initialization of the PWM generator of TIM4.
 * @param	None
 * @retval	None
 */
void PWM_Init(void) {
	/* PWM mode 2 = Clear on compare match */
    /* PWM mode 1 = Set on compare match */
    BUZZER_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    BUZZER_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    BUZZER_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
    TIM_OC1Init(TIM4, &BUZZER_OCInitStructure);
    TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Enable);
}

int main(void) {
	RCC_ClocksTypeDef RCC_Clocks;
	int thisNote = 0;

	SystemInit();
	PORT_Init();
    TIMER_Init();
    PWM_Init();
    RCC_GetClocksFreq(&RCC_Clocks);
    // Setting the SysTick interrupt to 1 ms
  	SysTick_Config((RCC_Clocks.HCLK_Frequency/1000));
    // SysTick_Config((SystemCoreClock/1000)+10);

	while (1) {
		// Play Monkey Island
		thisNote = 0;
		while(1){
			thisNote = (thisNote + 1) %  ARRAYCOUNT(monkey_melody);
			BUZZER_SetFrequency(monkey_melody[thisNote], monkey_noteDurations[thisNote]);
			// Pause between notes
			delay(monkey_noteDurations[thisNote]*0.25);
			// Test the button on every cycle, if pressed change melody
			if(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0)){
				while(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0));
				break;
			}
		}
		// Play Tetris Theme
		thisNote = 0;
		while(1){
			thisNote = (thisNote + 1) %  ARRAYCOUNT(tetris_melody);
			BUZZER_SetFrequency(tetris_melody[thisNote], (1000/tetris_noteDurations[thisNote])*1.25);
			// Pause between notes
			delay(tetris_noteDurations[thisNote]*2);
			// Test the button on every cycle, if pressed change melody
			if(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0)){
				while(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0));
				break;
			}
		}
		// Play Pirates of the Caribbean
		thisNote = 0;
		while(1){
			thisNote = (thisNote + 1) %  ARRAYCOUNT(pirates_melody);
			BUZZER_SetFrequency(pirates_melody[thisNote], (pirates_noteDurations[thisNote])*1.25);
			// Pause between notes
			delay(pirates_melody[thisNote]*0.05);
			// Test the button on every cycle, if pressed change melody
			if(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0)){
				while(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0));
				break;
			}
		}
    }
}
