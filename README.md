# README #
This repository is a collection of exercises produced by me during the Embedded Systems course at my unveristy for the development board of the STM STM32F4-Discovery.

You can find various types of projects developed both with the standard library that with the STD library v1.1 and HAL library v1.12 from STM and in particular:

#### Library ####
- halperiph_lib: HAL library v1.12 from STM used in some project
- stdperiph_lib: STD library v1.1 from STM used in some project

#### Template ####
- HALSTM32Template: HAL library v1.12 based project template
- HALRTOSTemplate: HAL library v1.12 based project template with RTOS
- STM32Template: STD library v1.1 based project template
- STDRTOSThread: STD library v1.1 based project template with Free-RTOS

#### Projects ####
- LEDRing: a simple example of a finite state machine and GPIO control
- MMelody: a simple example of use of timer for generate buzzer tone 
- I2CComm: example of use of I2C peripheral
- USARTComm: example of use of USART peripheral and printf with nanolib (use external code for printf)
- LEDGyroSpin: example of use of GYRO MEMS on the board 
- RTOSThread: example of Free-RTOS with thread and dynamic allocation of it
- USBKeybJoy: example of USB Device for implementation of simple Keyboard Joystick
- USBKeybLipsum: dummy Keyboard lipsum in USB
- USBVCPShell: complete project of a USB VCP Shell for the remote debug of a board over USB with Free-RTOS
