#include "stm32f4xx.h"
#include "stm32f4xx_hal_gpio.h"

#include "cmsis_os.h"

#define LED_GREEN	GPIO_PIN_12	// LED Verde (Left)
#define LED_ORANGE 	GPIO_PIN_13	// LED Arancio (Up)
#define LED_RED		GPIO_PIN_14	// LED Rosso (Right)
#define LED_BLUE 	GPIO_PIN_15	// LED Blu (Down)
#define LEDS 		(LED_GREEN | LED_ORANGE | LED_RED | LED_BLUE)

void threadGREEN(void *args);
void threadORANGE(void *args);
void threadRED(void *args);
void threadBLUE(void *args);
void threadMAIN(void *args);

osThreadDef(LEDG, threadGREEN, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
osThreadDef(LEDO, threadORANGE, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
osThreadDef(LEDR, threadRED, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
osThreadDef(LEDB, threadBLUE, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
osThreadDef(MAIN, threadMAIN, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);

void init(void);

void main(void *args)
{
	init();
	osThreadCreate (osThread(MAIN), NULL);
	osKernelStart();

	// In this loop i will enter if one thread will stop the kernel
	while(1){

	}
}

void init(void){
	GPIO_InitTypeDef GPIO_InitStruct;

	/* Enable the GPIO_LED Clock */
	__GPIOD_CLK_ENABLE();

	/* Configure the GPIO_LED pins */
	GPIO_InitStruct.Pin = LEDS;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FAST;

	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOD, LEDS, GPIO_PIN_RESET);
}

void threadGREEN(void *args){
	while(1){
		HAL_GPIO_TogglePin(GPIOD, LED_GREEN);
		HAL_Delay(250);
	}
}

void threadORANGE(void *args){
	while(1){
		HAL_GPIO_TogglePin(GPIOD, LED_ORANGE);
		HAL_Delay(500);
	}
}

void threadRED(void *args){
	while(1){
		HAL_GPIO_TogglePin(GPIOD, LED_RED);
		HAL_Delay(1000);
	}
}

void threadBLUE(void *args){
	while(1){
		HAL_GPIO_TogglePin(GPIOD, LED_BLUE);
		HAL_Delay(2000);
	}
}

void threadMAIN(void *args){

	osThreadCreate (osThread(LEDG), NULL);
	HAL_Delay(2000);
	osThreadCreate (osThread(LEDO), NULL);
	HAL_Delay(2000);
	osThreadCreate (osThread(LEDR), NULL);
	HAL_Delay(2000);
	osThreadCreate (osThread(LEDB), NULL);
	HAL_Delay(2000);

	while(1){

	}

}
