// Dependency: stdperiph_lib (Standard Library v1.1.0 STM32)

#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

#include "cmsis_os.h"

// Definition of mnemonic macro
#define LED_GREEN	GPIO_Pin_12	// LED Verde (Left)
#define LED_ORANGE 	GPIO_Pin_13	// LED Arancio (Up)
#define LED_RED		GPIO_Pin_14	// LED Rosso (Right)
#define LED_BLUE 	GPIO_Pin_15	// LED Blue (Down)
#define LEDS 		(LED_GREEN | LED_ORANGE | LED_RED | LED_BLUE)

void threadGREEN(void *args);
void threadORANGE(void *args);

osThreadDef(LEDG, threadGREEN, osPriorityNormal, 0, 128);
osThreadDef(LEDO, threadORANGE, osPriorityNormal, 0, 128);

void init(void);

void main(void *args)
{
	init();
	osThreadCreate (osThread(LEDG), NULL);
	osThreadCreate (osThread(LEDO), NULL);
	osKernelStart();

	// In this loop i will enter if one thread will stop the kernel
	while(1){

	}
}

void init(void){
	GPIO_InitTypeDef GPIO_InitStruct;

	/* GPIOD and GPIOA Peripheral clock enable */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOD, ENABLE);

	GPIO_StructInit(&GPIO_InitStruct);
	/* Configure PD12 and PD13 in output pushpull mode */
	GPIO_InitStruct.GPIO_Pin = LEDS;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOD, &GPIO_InitStruct);
}

void threadGREEN(void *args){
	while(1){
		GPIO_ToggleBits(GPIOD, LED_GREEN);
		osDelay(250);
	}
}

void threadORANGE(void *args){
	while(1){
		GPIO_ToggleBits(GPIOD, LED_ORANGE);
		osDelay(750);
	}
}
