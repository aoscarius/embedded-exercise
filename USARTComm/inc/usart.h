/**
* University of Naples Federico II
* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*/

#ifndef USART_H_
#define USART_H_

#include "stm32f4xx_usart.h"

// Comment to disable printf redirect
#define USART_PRINTF
// Comment to disable echo functionality
#define ECHO_ENABLED

// USART Configuration
#define USARTn 		USART1
#define GPIOn 		GPIOB
#define RCCUSARTn	RCC_APB2Periph_USART1
#define RCCGPIOn	RCC_AHB1Periph_GPIOB
#define USARTnTX	GPIO_Pin_6
#define USARTnRX	GPIO_Pin_7
#define USARTnTXs 	GPIO_PinSource6
#define USARTnRXs	GPIO_PinSource7
#define USARTnAF	GPIO_AF_USART1

// USART Prototypes
void USARTInit(uint16_t BaudRate);
void USART_putch(uint8_t ch);
uint8_t USART_getch();
uint16_t USART_writestr(uint8_t *buff, uint16_t len);
uint16_t USART_readstr(uint8_t *buff, uint16_t len);

#endif /* USART_H_ */
