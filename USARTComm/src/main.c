/**
* University of Naples Federico II
* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*/

/**
 * Questo progetto è un semplice testing della periferica USART del SOC STM32F407VG.
 * Vengono implementate le funzioni di getch e putch in polling. Si testa inoltre la
 * possibilità di fare overloading delle syscall usate dalla printf e dalla scanf,
 * _write e _read.
 */

// Dependency: stdperiph_lib (Standard Library v1.1.0 STM32)

#include <stm32f4xx.h>
#include "usart.h"
#include <stdio.h>
#include "printf.h"

// This library contain the macro for the color settings on terminal
#include "termcolor.h"

__IO uint32_t TDelay;

/**
 * @brief	This function implement the handler fot the SysTick interrupt. It count
 * 			the millisecond that passed from the set of TDelay variable and the 0. It work
 * 			if the SysTick interrupt is set correctly to 1ms trough the SysTick_Config.
 * @param	None
 * @retval	None
 */
void SysTick_Handler(void){
	if(TDelay!=0){
		TDelay--;
	}
}

/**
 * @brief	This implement a precise delay function in milliseconds.
 * @param	Time in milliseconds
 * @retval	None
 */
void delay(uint32_t ms) {
    TDelay = ms;
    while(TDelay!=0);
}

/* TODO: Per usare la libreria newlib nano aggiungere -specs=nano.specs
 * a Settings > MCU GCC Linker > Miscellaneous > Linker Flags. Tuttavia
 * la printf, continua a non funzionare correttamente.
 */

void putcf (void* f,char ch){
	USART_putch(ch);
}

int main(void) {

	char str[]="Hello World.\n";
	char str_in;
	char counter=0;

	init_printf(0, putcf);

	USARTInit(38400);
	SysTick_Config((SystemCoreClock/1000)+10);

	USART_writestr(str, sizeof(str));

	while (1){
		printf(TXTYLW "Insert [%d]: " TXTRST "\n", counter++);
		str_in=USART_getch();
		printf(BLDCYN "You" TXTRST " are insert: %c \n", str_in);
		delay(1000);
	}
}
