/**
* University of Naples Federico II
* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*/

* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*
* Project: Zybo GPIO Peripheral
*/

#include <usart.h>

/**
 * @brief	This function initialize the USART on SOC STM32F407VG.
 * @param	BaudRate is the baudrate of the USART comunication.
 * @retval	None
 */
void USARTInit(uint16_t BaudRate){
	GPIO_InitTypeDef GPIO_InitStruct;
	USART_InitTypeDef USART_InitStruct;
	NVIC_InitTypeDef NVIC_InitStructure;

	//Enable the clocks for the GPIOB and the USART
	RCC_APB2PeriphClockCmd(RCCUSARTn, ENABLE);
	RCC_AHB1PeriphClockCmd(RCCGPIOn, ENABLE);

	//Initialize pins GPIOB 6 and GPIOB 7
	GPIO_InitStruct.GPIO_Pin = USARTnTX | USARTnRX;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF; //we are setting the pin to be alternative function
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOn, &GPIO_InitStruct);

	//Connect the TX and RX pins to their alternate function pins
	GPIO_PinAFConfig(GPIOn, USARTnRXs, USARTnAF); //
	GPIO_PinAFConfig(GPIOn, USARTnTXs, USARTnAF);

	//Configure USART
	USART_InitStruct.USART_BaudRate = BaudRate;
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	USART_InitStruct.USART_Parity = USART_Parity_No;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx; //enable send and receive (Tx and Rx)
	USART_Init(USARTn, &USART_InitStruct);

	//Enable the interrupt
	USART_ITConfig(USARTn, USART_IT_RXNE, ENABLE);

	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	// finally this enables the complete USART1 peripheral
	USART_Cmd(USARTn, ENABLE);
}

/**
 * @brief	This function implement the putch in polling on USART
 * @param	ch is char to put on USART
 * @retval	None
 */
void USART_putch(uint8_t ch){
    if (ch=='\n') {
    	while (!(USARTn->SR & USART_SR_TC));
    	USARTn->DR = ((uint16_t)'\r' & (uint16_t)0x01FF);
    }
    while (!(USARTn->SR & USART_SR_TC));
    USARTn->DR = ((uint16_t)ch & (uint16_t)0x01FF);
}

/**
 * @brief	This function implement the getch in polling on USART
 * @param	None
 * @retval	char read from USART
 */
uint8_t USART_getch(){
    uint8_t ch='\0';
   	while (!(USARTn->SR & USART_SR_RXNE));
    ch = (uint8_t)(USARTn->DR & 0x01FF);
	#ifdef ECHO_ENABLED
    // echo functionality
    USART_putch(ch);
	#endif
    return ch;
}

/**
 * @brief	This function implement a simple print of string on USART
 * @param	buff is buffer where read the character to put on USART
 * @param	len is the length of the buffer
 * @retval	char write to USART
 */
uint16_t USART_writestr(uint8_t *buff, uint16_t len){
	int i;
	for(i=0; i<len; i++){
		USART_putch(buff[i]);
	}

	return i;
}

/**
 * @brief	This function implement a simple read of string on USART
 * @param	buff is buffer where read the character to put on USART
 * @param	len is the length of the buffer
 * @retval	char read from USART
 */
uint16_t USART_readstr(uint8_t *buff, uint16_t len){
	int i;
	for(i=0; i<len-1; i++){
		buff[i]=USART_getch();
		if (buff[i]=='\n') break;
	}
	buff[i+1]=0;

	return i;
}
