#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "lis3dsh.h"

#include "usbd_core.h"
#include "usbd_desc.h"
#include "usbd_hid.h"

#define ABS(x)	((x<0)?(-x):(x))

#define KEY_SCAN_ENTER	0x28 //Keyboard Return (ENTER)
#define KEY_SCAN_RIGHT	0x4F //Keyboard RightArrow
#define KEY_SCAN_LEFT	0x50 //Keyboard LeftArrow
#define KEY_SCAN_DOWN	0x51 //Keyboard DownArrow
#define KEY_SCAN_UP		0x52 //Keyboard UpArrow

#define THREADHOLDLow  -750
#define THREADHOLDHigh  750

static int16_t accXYZBuff[3] = {0};
static int8_t buttonPressed = 0;

/* Variables used for USB */
USBD_HandleTypeDef  hUSBDDevice;

// Function Prototype
uint32_t USBConfig(void);
void USBSendKey(uint8_t key_scan);
void BUTTONInit();
void LIS3DSHInit();
void SystemClock_Config(void);

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
	uint32_t delayValue = 0;

	/* STM32F4xx HAL library initialization:
	- Configure the Flash prefetch, instruction and Data caches
	- Configure the Systick to generate an interrupt each 1 msec
	- Set NVIC Group Priority to 4
	- Global MSP (MCU Support Package) initialization
	*/
	HAL_Init();

	SystemClock_Config();

	/* SysTick end of count event each 1ms */
	SystemCoreClock = HAL_RCC_GetHCLKFreq();
	SysTick_Config(SystemCoreClock / 1000);

	USBConfig();
	BUTTONInit();
	LIS3DSHInit();

	/* Infinite loop */
	while (1){
		if (buttonPressed==1){
			USBSendKey(KEY_SCAN_ENTER);
			buttonPressed=0;
		}
		if (accXYZBuff[0] < THREADHOLDLow) {
			USBSendKey(KEY_SCAN_LEFT);
		} else if (accXYZBuff[0] > THREADHOLDHigh) {
			USBSendKey(KEY_SCAN_RIGHT);
		} else if (accXYZBuff[1] < THREADHOLDLow) {
			USBSendKey(KEY_SCAN_DOWN);
		} else if (accXYZBuff[1] > THREADHOLDHigh) {
			USBSendKey(KEY_SCAN_UP);
		}
		accXYZBuff[0] = 0;
		accXYZBuff[1] = 0;
	}
}

/**
  * @brief  Initializes the USB for the demonstration application.
  * @param  None
  * @retval None
  */
uint32_t USBConfig(void)
{
  /* Init Device Library */
  USBD_Init(&hUSBDDevice, &HID_Desc, 0);
  
  /* Add Supported Class */
  USBD_RegisterClass(&hUSBDDevice, USBD_HID_CLASS);
  
  /* Start Device Process */
  USBD_Start(&hUSBDDevice);
  
  return 0;
}

void USBSendKey(uint8_t key_scan){
	uint8_t USBD_Keyb_Report[8] = {0};

	USBD_Keyb_Report[2] = key_scan;
    USBD_HID_SendReport (&hUSBDDevice, USBD_Keyb_Report, 8);
    HAL_Delay(50);
    USBD_Keyb_Report[2] = 0;
    USBD_HID_SendReport (&hUSBDDevice, USBD_Keyb_Report, 8);
    HAL_Delay(50);
}

void BUTTONInit(){
	GPIO_InitTypeDef GPIO_InitStruct;

	/* Enable the GPIO_LED Clock */
	__GPIOA_CLK_ENABLE();

	/* Configure the GPIO_LED pins */
	GPIO_InitStruct.Pin = GPIO_PIN_0;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FAST;

	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_NVIC_SetPriority(EXTI0_IRQn, 2, 0);
	HAL_NVIC_EnableIRQ(EXTI0_IRQn);
}

void LIS3DSHInit(){
	uint16_t ctrl = 0x0000;
	LIS3DSH_InitTypeDef l1s3dsh_InitStruct;

	/* Set configuration of LIS3DSH MEMS Accelerometer **********************/
	l1s3dsh_InitStruct.Output_DataRate = LIS3DSH_DATARATE_100;
	l1s3dsh_InitStruct.Axes_Enable = LIS3DSH_XYZ_ENABLE;
	l1s3dsh_InitStruct.SPI_Wire = LIS3DSH_SERIALINTERFACE_4WIRE;
	l1s3dsh_InitStruct.Self_Test = LIS3DSH_SELFTEST_NORMAL;
	l1s3dsh_InitStruct.Full_Scale = LIS3DSH_FULLSCALE_2;
	l1s3dsh_InitStruct.Filter_BW = LIS3DSH_FILTER_BW_800;

	/* Configure MEMS: power mode(ODR) and axes enable */
	ctrl = (uint16_t) (l1s3dsh_InitStruct.Output_DataRate | \
				   l1s3dsh_InitStruct.Axes_Enable);

	/* Configure MEMS: full scale and self test */
	ctrl |= (uint16_t) ((l1s3dsh_InitStruct.SPI_Wire    | \
					 l1s3dsh_InitStruct.Self_Test   | \
					 l1s3dsh_InitStruct.Full_Scale  | \
					 l1s3dsh_InitStruct.Filter_BW) << 8);

	/* Configure the accelerometer main parameters */
	LIS3DSH_Init(ctrl);
}

void HAL_SYSTICK_Callback(void){
	LIS3DSH_ReadACC(accXYZBuff);
}

/**
  * @brief  EXTI line detection callbacks.
  * @param  GPIO_Pin: Specifies the pins connected EXTI line
  * @retval None
  */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == GPIO_PIN_0)
	{
		buttonPressed = 1;
	}
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 168000000
  *            HCLK(Hz)                       = 168000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 4
  *            APB2 Prescaler                 = 2
  *            HSE Frequency(Hz)              = 8000000
  *            PLL_M                          = 8
  *            PLL_N                          = 336
  *            PLL_P                          = 2
  *            PLL_Q                          = 7
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 5
  * @param  None
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;

  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();
  
  /* The voltage scaling allows optimizing the power consumption when the device is 
     clocked below the maximum system frequency, to update the voltage scaling value 
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  
  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);
  
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;  
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5);

  /* STM32F405x/407x/415x/417x Revision Z devices: prefetch is supported  */
  if (HAL_GetREVID() == 0x1001)
  {
    /* Enable the Flash prefetch */
    __HAL_FLASH_PREFETCH_BUFFER_ENABLE();
  }
}
